var gulp = require("gulp");
var sass = require('gulp-sass');


gulp.task("compilescss", function () {
  return gulp
    .src("styles/scss/**/*.scss") // Get source files with gulp.src
    .pipe(sass()) // Sends it through a gulp plugin
    .pipe(gulp.dest("styles/css/")); // Outputs the file in the destination folder
});
